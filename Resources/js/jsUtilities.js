var getAppData = {
	getProjectList : function() {
		var ajax = Ti.Network.createHTTPClient();
		ajax.onreadystatechange = function() {
			if (this.readyState == 4) {
				var jdata = JSON.parse(this.responseText);
				projectListArray = jdata;
				
				/*
				 * Go ahead and populate the project select drop-down(s)
				 * because they won't get populated on first run otherwise.
				 */
				//$('#projectPickerTimer').append($('<option></option>').attr('selected','selected').attr('value','0').text('Select a project'));
				for(var a=0;a<=projectListArray.length-1;a++) {
					projectTitle = projectListArray[a].Title;
					projectID    = projectListArray[a].ProjectID;
					$('#projectPickerTimer').append($('<option></option>').attr('value',projectID).text(projectTitle));
				}	
			}
		}
		ajax.open('POST', SITE_URL+'projects/ProjectView/getProjectsForSelect');
		ajax.send({
			'authKey': AUTH_KEY
		});
	},
	getTaskListForSelect : function(projectID,selectObj,selectedTaskID) {
		var ajax = Ti.Network.createHTTPClient();
		ajax.onreadystatechange = function() {
			if (this.readyState == 4) {
				var jdata = JSON.parse(this.responseText);
				
				var platform = Ti.getPlatform();
				
				selectObj.empty();
    			var taskTitle,taskID,msTitle,msID,msArray = [],taskArray = [],a,b;
    			if (jdata.length>0) {
    				selectObj.append($('<option></option>').attr('value','0').text('Select a task'));
	    			for(a=0;a<=jdata.length-1;a++) {
						msArray   = jdata[a];
						taskArray = jdata[a].Tasks;
						msTitle   = jdata[a].Title;
						msID      = jdata[a].TaskID;
						if (platform == 'osx') {
							selectObj.append($('<option></option>').attr('value','0').text('< Milestone: '+msTitle+' >'));
						} else {
							selectObj.append($('<option class="gray"></option>').attr('value','0').text(msTitle));
						}
						for(b=0;b<=taskArray.length-1;b++) {
							taskTitle = stringUtil.txtToField(taskArray[b].Title);
							taskID    = taskArray[b].TaskID;
							var optionObj = $('<option></option>').attr('value',taskID).text(taskTitle);
							if (selectedTaskID == taskID) {
								optionObj.attr('selected', 'selected');
							}
							selectObj.append(optionObj);
						}
					}	
	    		} else {
	    			selectObj.append('<option></option>');
	    		}	
				if (activeTimerObj.taskTitle != '' && activeTimerObj.taskID == 0) {
					/*
					 * Then we've entered a new task.
					 */
					selectObj.append('<option value="00" selected>'+activeTimerObj.taskTitle+'</option>');
				}
			}
		}
		ajax.open('POST', SITE_URL+'tasks/TaskView/getTasksForSelect/0/json');
		ajax.send({
			'authKey': AUTH_KEY,
			'projectID': projectID
		});
	}
}

var stringUtil = {
	txtToField : function(string) {
	    /*
	     * Replace <br>, <br />
	     */
	    var newString  = string.replace(/<br>/g,"\r\n");
	    newString = newString.replace(/<br\/>/g,"\r\n");
	
	    /*
	     * Replace ' and " html codes with actual characters
	     */
	    newString = newString.replace(/&#39;/g,"'");
	    newString = newString.replace(/&#34;/g,'"');
	    newString = newString.replace(/&quot;/g,'"');
	    newString = newString.replace(/&amp;/g,'&');
	        
	    /*
	     * Remove any other HTML tags like links
	     */
	    newString = stringUtil.stripTags(newString);
	    return newString;
	},
	stripTags : function(string) {
		return string.replace(/<([^>]+)>/g,'');
	},
	escapeString : function(string) {
		var string2 = string.replace('"','\"');
		var string3 = string2.replace("'","\'");
		return string3;
	}
}
