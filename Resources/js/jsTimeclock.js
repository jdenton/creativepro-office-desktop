var timer = {
	loadWidgetTimer : function() {
		/*
		 * Load up project picker
		 */		
		renderTimer.renderWidgetTimer();
		timer.attachEventHandlers();
		
		var today = moment().format('dddd, MMMM Do YYYY');
		$('#widgetTitleBarTimer').html(today);
		/*
		var timerCal = $('#widgetTitleBarTimer').datePicker({
			format: 'MM/DD/YYYY'
		});
				
		timerCal.on('select', function() {
			var date = this.element.value;
			var headerDate = moment(date).format('dddd, MMMM Do YYYY');
			var timersDate = moment(date).format('YYYY-MM-DD');
			
			if (timerSwitches.timersRunning == true) {
				noty({
					text: 'Please stop your timer first.',
					type: 'alert'
				});
			} else {
				$('#widgetTitleBarTimer').html(headerDate);		
				utilsTimer.getSavedTimers(timersDate);				
			}		
		});
		*/
		
		var date = moment().format('YYYY-MM-DD');
		utilsTimer.getSavedTimers(date);
	},
	attachEventHandlers : function() {
		$('#widgetTitleBar').click(function() {
			
		});		
		
		$('#timerElapsedTime').keydown(function() {
			timerSwitches.changedElapsedTime = true;
		});
		
		$('#buttonStartTimer').click(function() {
			timer.saveTimerForm();
			$('#timerForm').slideUp('fast');
		});
		
		$('#buttonSaveTimer').click(function() {
			timer.saveTimerForm(true);
			utilsTimer.clearForm();
			$('#timerForm').slideUp('fast');
		});
		
		$('#buttonCancelTimer').click(function() {
			utilsTimer.clearForm();
			$('#timerForm').slideUp('fast');
		});
		
		$('#timerList').delegate('.timerControlButton', 'click', function() {
			var button = $(this);
			var timerID = button.parent().parent().parent().data('timerid');
			var timerDisplay = button.parent().find('.timeDisplay');
        	if (button.hasClass('buttonStop')) {
        		// STOP
				utilsTimer.timerStop(timerID,activeTimerObj,true);
				timerSwitches.timersRunning == false;
			} else {
				// START
				utilsTimer.timerStart(timerID);
			}
		});
		
		$('#timerList').delegate('.timerRemove', 'click', function() {
			var button = $(this);
			var timerID = button.parent().parent().parent().data('timerid');
			utilsTimer.removeTimerRow(timerID);
		});	
	},	
	saveTimerForm : function(toServer) {
		/*
		 * This is where we go from the timer form.  
		 * If we save a new timer or update an existing one.
		 */
		var now = moment();
		var timerID;
		
		if (timerSwitches.newTimer == true) {
			timerID = 0;
		} else {
			timerID = timerSwitches.activeTimerID;
		}
		
		var projectID    = $('#projectPickerTimer').val();
		var projectTitle = $('#projectPickerTimer option:selected').text();
		var taskID       = $('#taskPickerTimer').val();
		var taskTitle    = $('#taskPickerTimer option:selected').text();
		var comments     = $('#timerComments').val();
		var elapsedTime  = $('#timerElapsedTime').val();
		var dateTime     = now.format('YYYY-MM-DD');
		var timesheetID  = $('#timesheetID').val();
		var time         = new Date().getTime();
		
		var clientCo     = '';
		$.each(projectListArray, function() {
			var projectObj = this;
			if (projectObj.ProjectID == projectID) {
				clientCo = projectObj.Company;
			}
		});
		
		if (taskID == '0' && taskID != '00') {
			taskTitle = '';
		}
		if (isNaN(taskID)) {
			taskID = '00';
		}
		
		var billable = '1';
		if ($('#timerBillable').attr('checked') != 'checked') {
			billable = '0';
		}
		
		var timerObj = {
			timerID:      timerID,
			projectID:    projectID,
			taskID:       taskID,
			projectTitle: projectTitle,
			taskTitle:    taskTitle,
			comments:     comments,
			elapsedTime:  elapsedTime,
			billable:     billable,
			dateTime:     dateTime,
			clientCo:     clientCo,
			timeSheetID:  timesheetID
		}
		
		timerID = timer.saveTimer(timerObj,toServer);		
		
		if (timerSwitches.newTimer == true) {
			timerObj.timerID = timerID;
		} else {
			utilsTimer.deleteTimerObj(timerObj);
		}
		activeTimerCollection.push(timerObj);
		
		timerSwitches.activeTimerID = timerID;
		activeTimerObj = timerObj;
		
		renderTimer.renderTimerRow();
		
		if (timerSwitches.timersRunning == true) {
			utilsTimer.stopOtherTimers(timerID);
		}	
		
		if (toServer != true) {			
			utilsTimer.timerStart(timerID);
		}	
	},	
	saveTimer : function(timerObj,toServer) {
		var now = moment();
		if (timerObj == null || typeof timerObj == 'undefined') {
			timerObj = utilsTimer.getActiveTimerObj();
		}	
		
		var timerID      = timerObj.timerID;
		var projectID    = timerObj.projectID;
		var projectTitle = stringUtil.escapeString(timerObj.projectTitle);
		var taskID       = timerObj.taskID;
		var taskTitle    = stringUtil.escapeString(timerObj.taskTitle);
		var elapsedTime  = timerObj.elapsedTime;
		var billable     = timerObj.billable;
		var comments     = stringUtil.escapeString(timerObj.comments);
		var dateTime     = now.format('YYYY-MM-DD');
		var time         = new Date().getTime();
		var clientCo     = stringUtil.escapeString(timerObj.clientCo);
		var timesheetID  = timerObj.timeSheetID;
		
		var db = Ti.Database.open('cpo');
		if (timerID == 0) {
			var sql = "INSERT INTO cpo_timesheet (ProjectID, ProjectTitle, TaskID, TaskTitle, Start, ElapsedTime, DateClockStart, DateClockEnd, Billable, Comments, ClientCo, Sync) values (?,?,?,?,?,?,?,?,?,?,?,0)";
			db.execute(sql, projectID, projectTitle, taskID, taskTitle, time, elapsedTime, dateTime, dateTime, billable, comments, clientCo);
			timerID = db.lastInsertRowId;
			timerObj.timerID = timerID;
		} else {
			var sql  = "UPDATE cpo_timesheet SET ProjectID = ?, ProjectTitle = ?, TaskID = ?, TaskTitle = ?, Stop = ?, ElapsedTime = ?, DateClockEnd = ?, Billable = ?, Comments = ?, ClientCo = ?, Sync = '0'"; 
			db.execute(sql, projectID, projectTitle, taskID, taskTitle, time, elapsedTime, dateTime, billable, comments, clientCo)
			
		}
		db.close();
		
		if (toServer == true) {
			var ajax = Ti.Network.createHTTPClient();
			ajax.onreadystatechange = function(status, response) {
				if (this.readyState == 4) {
					var jdata = JSON.parse(this.responseText);
					var status = jdata.Status;
					var message = jdata.Message;
					var timeSheetID = jdata.TimesheetID;
					var taskID      = jdata.NewTaskID;					
					utilsTimer.deleteTimerObj(timerObj);
					timerObj.timeSheetID = timeSheetID;
					activeTimerCollection.push(timerObj);
					
					var db = Ti.Database.open('cpo');
					var sql = "UPDATE cpo_timesheet SET TaskID = '"+taskID+"', Sync = '"+timeSheetID+"' WHERE ID = '"+timerID+"'";
					db.execute(sql); 
					db.close();
				}	
			}	
			ajax.open('POST',SITE_URL+'timesheets/TimesheetView/saveTimesheetRecordFromJobTimer');
			ajax.send({
				'authKey':     AUTH_KEY,
				'action':      'saveFromDesktop',
		        'projectID':   projectID,
		        'taskID':      taskID,
		        'taskTitle':   taskTitle,
		        'comments':    comments,
		        'billable':    billable,
		        'elapsedTime': elapsedTime,
		        'dateTime':    dateTime,
		        'timeSheetID': timesheetID
			});
		}
		
		return timerID;
	},
	controlTimer : function(timerID,action) {
		if (timerSwitches.timersRunning == true) {
            //widgetToolsObj.widgetTimerCheckRunningTimers();
        }
        
        var currentTime, currentTimeElement;
        
        var row = utilsTimer.getRowObjFromTimerID(timerID);
        var currentTimeElement = row.find('.timeDisplay');
        var currentTime = currentTimeElement.text();
        
		var timeArray = currentTime.split(':');
		var sec  = timeArray[2];
		var min  = timeArray[1];
		var hour = timeArray[0];
        
        if (sec.substr(0,1) == '0') {
            sec = sec.substr(1,1);
        }
        if (min.substr(0,1) == '0') {
            min = min.substr(1,1);
        }
        if (hour.substr(0,1) == '0') {
            hour = hour.substr(1,1);
        }

        sec  = parseInt(sec);
		min  = parseInt(min);
		hour = parseInt(hour);

		if (action == 'start') {
            sec++;
			if (sec == 60) {
				sec = 0;
				min = min + 1;
			}

			if (min == 60) {
			   min = 0;
			   hour = hour + 1;
			}
			
			var cookieTime = hour+':'+min+':'+sec;
			if (sec<=9) { sec = "0" + sec; }
            if (min<=9) { min = "0" + min; }
            if (hour<=9) { hour = "0" + hour; }
            var jobTime = hour+':'+min+':'+sec;

			/*
			 * Render new time to window
			 */
			currentTimeElement.html(jobTime);
            eval("timerTimeout"+timerID+" = window.setTimeout(\"timer.controlTimer("+timerID+",'start');\", 1000);");
		} else {
			try {
				eval("window.clearTimeout(timerTimeout"+timerID+");");
			} catch(err) {
			}	
		}
	}
}	

var renderTimer = {
	renderTimerRow : function(initState) {
		var timerID = activeTimerObj.timerID;
		var taskTitle = '';
		if (activeTimerObj.taskTitle != '') {
			taskTitle = ' : '+activeTimerObj.taskTitle;
		}
		if (timerSwitches.newTimer == false) {
			/*
			 * Then update existing row
			 */
			var row = utilsTimer.getRowObjFromTimerID(timerID);
			
			if (timerSwitches.changedElapsedTime == true) {
				elapsedTimeClock = utilsTimer.elapsedTimeToClock();
				row.find('.timeDisplay').html(elapsedTimeClock);
			}
	        
	        row.find('.clientName').html(activeTimerObj.clientCo);
	        row.find('.project-task').html(activeTimerObj.projectTitle+taskTitle);
	        row.find('.comments').html(activeTimerObj.comments);
		} else {
			/*
			 * Add new row
			 */
			var elapsedTimeClock = '00:00:00';
			if (activeTimerObj.elapsedTime != '') {
				elapsedTimeClock = utilsTimer.elapsedTimeToClock();
			}
			
			var outArray = [];
			outArray.push('<div class="timerRow" data-timerid="'+timerID+'">');
			outArray.push('<div class="timerRowInner">');
			outArray.push('<div class="timerRowLeft">');
				outArray.push('<p class="clientName listGreyBold">'+activeTimerObj.clientCo+'</p>');
				outArray.push('<p class="project-task listNormal">'+activeTimerObj.projectTitle+taskTitle+'</p>');
				outArray.push('<p class="comments listGreySmall">'+activeTimerObj.comments+'</p>');
			outArray.push('</div>');
			outArray.push('<div class="timerRowRight">');
				outArray.push('<p class="listBlackBold timeDisplay">'+elapsedTimeClock+'</p>');
				outArray.push('<button class="buttonRemove timerRemove" title="Remove this timer" />');
				if (initState == 'stopped') {
					outArray.push('<button class="buttonPlay timerControlButton" title="Start timer" style="float: left;" />');
				} else {
					outArray.push('<button class="buttonStop timerControlButton" title="Stop timer" style="float: left;" />');
				}	
			outArray.push('</div>');
			outArray.push('<div class="clearfix"></div>');
			outArray.push('</div></div>');
			var outString = outArray.join('');
			$('#timerList').prepend(outString);		
		}
	},
	renderWidgetTimer : function() {
		$('#widgetContent_timer').show();
		
		$('#projectPickerTimer').change(function() {
			var selectObj = $('#taskPickerTimer');
			var projectID = $(this).val();
			if (projectID>0) {
				getAppData.getTaskListForSelect(projectID,selectObj);
			}
		});
		
		$('#new').click(function(e) {
			renderTimer.renderTimerForm();
			timerSwitches.newTimer = true;
			utilsTimer.clearForm();
		});
		
		$(document).bind('keydown', 'ctrl+n',function (evt){
            renderTimer.renderTimerForm();
			timerSwitches.newTimer = true;
			utilsTimer.clearForm();
			return false;
        });
        
        $(document).bind('keydown', 'ctrl+s',function (evt){
            timer.saveTimerForm();
			$('#timerForm').slideUp('fast');
			return false;
        });
        
        $(document).bind('keydown', 'ctrl+p',function (evt){
            timer.saveTimerForm();
			$('#timerForm').slideUp('fast');
			return false;
        });
	},
	renderTimerForm : function() {
		timerSwitches.changedElapsedTime = false;
		
		var projectTitle,projectID;
		/*
		if ($('#projectPickerTimer option').length<2) {
			$('#projectPickerTimer').append($('<option></option>').attr('selected','selected').attr('value','0').text('Select a project'));
			for(var a=0;a<=projectListArray.length-1;a++) {
				projectTitle = projectListArray[a].Title;
				projectID    = projectListArray[a].ProjectID;
				$('#projectPickerTimer').append($('<option></option>').attr('value',projectID).text(projectTitle));
			}	
		}
		*/		
		
		if (timerSwitches.newTimer == false) {
			/*
			 * Render contents of activeTimerObj in form
			 */
			$('#projectPickerTimer').val(activeTimerObj.projectID);
			$('#timerComments').val(activeTimerObj.comments);
			$('#timerElapsedTime').val(activeTimerObj.elapsedTime);
			
			$('#timesheetID').val(activeTimerObj.timeSheetID);
			getAppData.getTaskListForSelect(activeTimerObj.projectID,$('#taskPickerTimer'),activeTimerObj.taskID);
			
			if (activeTimerObj.billable == 1) {
				$('#timerBillable').attr('checked', 'checked');	
			} else {
				$('#timerBillable').removeAttr('checked');			
			}
			
			$('#buttonSaveTimer').show();
			
		} else {
			$('#buttonSaveTimer').hide();
		}
		var container = $('#timerForm');
			
		if (container.is( ":visible" )) {
			container.slideUp('fast');
		} else {
			container.slideDown('fast');
			$('#projectPickerTimer').focus();
		}
	}
}

var utilsTimer = {
	clearForm : function() {
		//$('#projectPickerTimer').val('');
		$('#projectPickerTimer').eq(0).attr('selected', 'seleted');
		$('#taskPickerTimer').val('');
		$('#timerComments').val('');
		$('#timerElapsedTime').val('');
		$('#timerBillable').attr('checked', 'checked');
		$('#timerProjectTaskReadOnly').hide().html('');
		$('#timerProjectTaskSelect').show();
		timerSwitches.changedElapsedTime = false;
	},
	elapsedTimeDecimal : function(elapsedTime) {
		var elapsedTimeArray = elapsedTime.split(':');
		var hour = elapsedTimeArray[0];
		var min  = elapsedTimeArray[1];
		var sec  = elapsedTimeArray[2];

		if (sec.substr(0,1) == '0') {
            sec = sec.substr(1,1);
        }
        if (min.substr(0,1) == '0') {
            min = min.substr(1,1);
        }
        if (hour.substr(0,1) == '0') {
            hour = hour.substr(1,1);
        }

        sec  = parseInt(sec);
		min  = parseInt(min);
		hour = parseInt(hour);
		
		if (sec>29) { min = min+1; }
		var minDecimal = parseFloat(min/60);
		var elapsedTimeDecimal = parseFloat(hour+minDecimal);
		elapsedTimeDecimal = elapsedTimeDecimal.toFixed(2);
		return elapsedTimeDecimal;
	},
	elapsedTimeToClock : function() {
		var elapsedTime = parseFloat(activeTimerObj.elapsedTime);
		var elapsedClock;
		if (isNaN(elapsedTime)) {
			return;
		} else {
			var hours   = Math.floor(elapsedTime);
			var minutes = Math.round(60 * (elapsedTime - hours));
			if (hours<10) {
				hours = '0' + hours;
			}
			if (minutes<10) {
				minutes = '0' + minutes;
			}
			return hours+':'+minutes+':00';
		}
	},
	removeTimerRow : function(timerID) {
		$('.timerRow').each(function(index) {		
			var row = $(this);			
			if (row.data('timerid') == timerID) {
				row.fadeOut('fast');
			}
		});	
		
		/*
		 * Delete record from database
		 */
		var db = Ti.Database.open('cpo');
		db.execute("DELETE FROM cpo_timesheet WHERE ID = "+timerID); 
	},
	timerObjFromTimerID : function(timerID,database) {
		if (database == true) {
			/*
			 * Then create timerObj from database
			 */
		} else {
			/*
			 * Then create timerObj from timerRow
			 */
		}
	},
	getRowObjFromTimerID : function(timerID) {
		if (timerID == null || timerID == '') {
			timerID = activeTimerObj.timerID;
		}
		var row;
		$('.timerRow').each(function() {
        	var timerRow = $(this);
        	if (timerRow.data('timerid') === timerID) {
        		row = $(this);
        	}	
        });	
        return row;
	},
	getActiveTimerObj : function() {
		/*
		 * Get whatever timer object is currently active
		 */
		var obj = [];
		$.each(activeTimerCollection, function(i, v) {
			if (v.timerID == timerSwitches.activeTimerID) {
				obj = v;
			}
		});
		return obj;
	},
	setActiveTimerObj : function(timerID) {
		/*
		 * Set the timer object that we want to be currently active
		 */
		timerSwitches.activeTimerID = timerID;
		$.each(activeTimerCollection, function(i, v) {
			if (v.timerID == timerID) {
				activeTimerObj = v;
			}
		});
	},
	stopOtherTimers : function(timerID) {
		$.each(activeTimerCollection, function(i, v) {
			if (v.timerID != timerID) {
				utilsTimer.timerStop(v.timerID,v,false);
			}
		});
	},
	timerStop : function(timerID,timerObj,fromTimer) {
		var row = utilsTimer.getRowObjFromTimerID(timerID);
		var button = row.find('.timerControlButton');
		var timerDisplay = row.find('.timeDisplay');
		button.removeClass('buttonStop').addClass('buttonPlay');
		button.attr('title','Start timer');
		timer.controlTimer(timerID,'stop');		
		timerObj.elapsedTime = utilsTimer.elapsedTimeDecimal(timerDisplay.html());
		timer.saveTimer(timerObj);
		if (fromTimer == true) {
			timerSwitches.timersRunning = false;
			timerSwitches.newTimer = false;
			renderTimer.renderTimerForm();
		}
		Ti.UI.setBadge('');
		tray.setIcon("app://images/icon16.png");
	},
	timerStart : function(timerID) {
		if (timerSwitches.timersRunning == true) {
			utilsTimer.stopOtherTimers(timerID);
		}
	
		timerSwitches.timersRunning = true;
		var row = utilsTimer.getRowObjFromTimerID(timerID);
		var button = row.find('.timerControlButton');
		
		button.removeClass('buttonPlay').addClass('buttonStop');
		timer.controlTimer(timerID,'start');
		button.attr('title','Stop timer');
		$('#timerForm').slideUp('fast');
		utilsTimer.setActiveTimerObj(timerID);
		
		Ti.UI.setBadge('T');
		tray.setIcon("app://images/icon16Arrow.png");
	},
	deleteTimerObj : function(timerObj) {
		$.each(activeTimerCollection, function(i, v) {
			if (v.timerID == timerObj.timerID) {
				activeTimerCollection.splice(i,1);
			}
		});
	},
	getSavedTimers : function(date) {
		/*
		 * This method gets called when we first open the app 
		 * and we need to get any timers that were alreay entered
		 * for today.
		 */
		timerSwitches.newTimer = true;
		timerSwitches.activeTimerID = 0;
		timerSwitches.timerRunning = '';
		timerSwitches.changedElapsedTime = false;
		
		
		$('#timerList').html('');
		var appDB = Ti.Database.open('cpo');
		var sql = "SELECT * FROM cpo_timesheet WHERE DateClockEnd = '"+date+"'";
		var rows = appDB.execute(sql);		
		var rowCount = rows.rowCount();
		appDB.close();
		
		if (rowCount<1) {
			noty({
				text: 'There are no timers for today.',
				type: 'alert',
				timeout: 3000
			});	
		} else {		
			while (rows.isValidRow()) {
				var timerObj = {
					timerID:      rows.fieldByName('ID'),
					projectID:    rows.fieldByName('ProjectID'),
					taskID:       rows.fieldByName('TaskID'),
					projectTitle: rows.fieldByName('ProjectTitle'),
					taskTitle:    rows.fieldByName('TaskTitle'),
					comments:     rows.fieldByName('Comments'),
					elapsedTime:  rows.fieldByName('ElapsedTime'),
					billable:     rows.fieldByName('Billable'),
					dateTime:     rows.fieldByName('DateClockEnd'),
					clientCo:     rows.fieldByName('ClientCo'),
					timeSheetID:  rows.fieldByName('Sync')
				}
				activeTimerCollection.push(timerObj);
				activeTimerObj = timerObj;
				renderTimer.renderTimerRow('stopped');		
				activeTimerObj = [];	
				
				rows.next();
			}
		}		
	}
}
