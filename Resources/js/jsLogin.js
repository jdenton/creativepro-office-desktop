var login = {
	tryAutoLogin : function() {
		var db = Ti.Database.open('cpo');
		var rows = db.execute("SELECT * FROM cpo_login LIMIT 1");
		while (rows.isValidRow()) {
			var userid   = rows.fieldByName('Email');
			var password = rows.fieldByName('Password');
			var authKey  = rows.fieldByName('AuthKey');
			var userType = rows.fieldByName('UserType');
			var permissions = rows.fieldByName('Permissions');
			rows.next();
		}		
		db.close();
		Ti.API.info('hi there');
		
		/*
		 * Try logging in if we have a userid
		 */
		if (userid != '' && userid != null) {
			login.tryLoginAtServer(userid,password);	
		} else {
			login.renderLoginView();
		}		
	},
	renderLoginView : function() {
		$('#appContent').hide();
		$('#loginContainer').show();
		$('#buttonLogin').click(function() {
			login.tryLogin();
		});
	},
	tryLogin : function() {
		var userid    = $('#userid').val();
		var password  = $('#password').val();
		var saveLogin = $('#saveLogin').attr('checked'); 
		
		if (saveLogin == 'checked') {
			saveLogin = true;
		} else {
			saveLogin = false;
		}
		
		if (userid == '' || password == '') {
			noty({
				text: 'Please enter email & password.',
				type: 'error'
			});
		} else {		
			login.tryLoginAtServer(userid,password,saveLogin);
		}	
	},
	tryLoginAtServer : function(userid,password,saveLogin) {
		var ajax = Ti.Network.createHTTPClient();
		ajax.onreadystatechange = function(status, response) {
			if (this.readyState == 4) {
				var jdata = JSON.parse(this.responseText);
				if (jdata.Error == '1') {
	    			noty({
						text: 'Cannot find your account.',
						type: 'error'
					});
	    		} else {
	    			AUTH_KEY  = jdata.AuthKey;
	    			USER_TYPE = jdata.UserType;
	    			if (saveLogin == true) {
	    				jdata.Email = userid;
	    				jdata.Password = password;
		    			login.saveCredentials(jdata);
		    		}	
		    		
		    		noty({
						text: 'Logging in...',
						type: 'success',
						timeout: 500
					});
		    		
		    		app.spinUp();
		    	}	
			}
		}
		ajax.open('POST', SITE_URL+'login/tryLogin/1');
		ajax.send({
			'userid': userid,
			'password' : password
		});
	},
	saveCredentials : function(jdata) {
		var email       = jdata.Email;
		var password    = jdata.Password;
		var authKey     = jdata.AuthKey;
	    var userType    = jdata.UserType;
	    var permissions = jdata.Permissions;
	    
	    var db = Ti.Database.open('cpo');
	    db.execute('INSERT INTO cpo_login (Email, Password, AuthKey, Permissions, UserType, KeepLoggedIn) values (?,?,?,?,?,1)', email, password, authKey, permissions, userType);
		db.close();
	},
	logout : function() {
		var db = Ti.Database.open('cpo');
	    db.execute('DELETE FROM cpo_login');
		db.close();
		
		app.hideAllWidgets();
		$('#footerBar').hide();
		$('#loginContainer').show();
	}
}
