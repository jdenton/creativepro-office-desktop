var tasks = {
	loadWidgetTasks : function() {
		var ajax = Titanium.Network.createHTTPClient();
		ajax.onerror = function(e) {
    		alert('Error');
		};
		ajax.onload = function() {
    		var data = this.responseText;
    		var jdata = JSON.parse(data);
    		tasks.renderWidgetTasks(jdata);
		};
		ajax.open('POST', SITE_URL+'tasks/TaskView/getTaskDigestView/json/0');
		ajax.send({
			'authKey': AUTH_KEY
		});
	},
	renderWidgetTasks : function(jsonObject) {
		var jsonObject = jsonObject[0];1
        var totalTasksOverdue, totalTasksToday, totalTasksTomorrow, totalTasksThisWeek, totalTasksNextWeek, totalTasks = 0;
        if (jsonObject.TasksOverdue) {
            totalTasksOverdue  = jsonObject.TasksOverdue.length;
        }
        if (jsonObject.TasksToday) {
            totalTasksToday    = jsonObject.TasksToday.length;
        }
        if (jsonObject.TasksTomorrow) {
            totalTasksTomorrow = jsonObject.TasksTomorrow.length;
        }
        if (jsonObject.TasksThisWeek) {
            totalTasksThisWeek = jsonObject.TasksThisWeek.length;
        }
        if (jsonObject.TasksNextWeek) {
            totalTasksNextWeek = jsonObject.TasksNextWeek.length;
        }        

        var panelDisplayDefault = 'block';
        if (jsonObject.TotalTasks>10) {
            panelDisplayDefault = 'none';
        }
        var stringOut = [], header;

        stringOut.push('<div class="boxBlue" style="margin-bottom: 6px;"><div style="padding: 3px;">');
		stringOut.push('<div class="taskControlContainer">');
            stringOut.push('<a href="#" class="icon_tasks_small_add linkNewTask">New Task</a>');
        stringOut.push('</div>');

            /*
             * New quick task form
             */
            /*
             * Set current date
             */
            var today = new Date();
            var todayDate = today.toString('M/dd/yyyy');
            var todayTime = today.toString('h:mm tt');
            stringOut.push('<div class="taskFormContainer" style="width: 99%; display: none;">');
            stringOut.push('<input type="hidden" class="actionTask" value="add" />');
            stringOut.push('<input type="hidden" class="itemIDTask" value="" />');
            stringOut.push('<div class="padBottom6"><strong>Task:</strong><br /><input type="text" class="taskTitle saverFieldTask" style="width: 95%;" /></div>');
            stringOut.push('<div class="padBottom6"><strong>When:</strong><br /><input class="taskDateDueWidget saverFieldTask" type="text" style="width: 100px;" value="'+todayDate+'" /> &nbsp;<strong>at</strong>&nbsp;&nbsp;');
            stringOut.push('<input class="taskTimeDue clearOnFocus saverFieldTask" type="text" style="width: 70px;" value="'+todayTime+'" /></div>');
            stringOut.push('<div class="padBottom6"><strong>Select Project:</strong><br /><select class="taskProjectSelect" style="width: 97%;"></select></div>');
            stringOut.push('<div class="padBottom6"><strong>Select Milestone:</strong><br /><select class="taskMilestoneSelect" disabled="disabled" style="width: 97%;"></select></div>');
            stringOut.push('<button class="smallButton saveTask" style="float: left; margin-right: 6px;"><span class="save">Save</span></button>');
            stringOut.push('<button class="smallButton cancelTask" style="float: left;"><span class="cancel">Cancel</span></button>');
            stringOut.push('<div style="clear: left;"></div>');
            stringOut.push('</div>');
            stringOut.push('</div></div>');

		var controlDisplay;
		if (totalTasksOverdue>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksOverdue);
		} else {
			controlDisplay = 'none';
		}
		var tasksOverdue = jsonObject.TasksOverdue;
		header       = jsonObject.TasksOverdueHeader;

        var status = 'overdue';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="display: '+controlDisplay+';">');
        stringOut.push('<div class="status_past_due" style="margin: 3px; float: left;"><strong class="redText"><span id="'+status+'_totalTasks" class="redText">'+totalTasksOverdue+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksOverdue)-1);a++) {
			stringOut.push(tasks.renderWidgetTaskElement(tasksOverdue[a],status));
		}
		stringOut.push('</div>');
		
        if (totalTasksToday>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksToday);
		} else {
			controlDisplay = 'none';
		}
		var tasksToday = jsonObject.TasksToday;
		header         = jsonObject.TasksTodayHeader;

        status = 'today';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="margin-top: 6px; display: '+controlDisplay+';">');
        stringOut.push('<div class="icon_calendar_today" style="margin: 3px; float: left;"><strong><span id="'+status+'_totalTasks">'+totalTasksToday+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="border-top: none; display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksToday)-1);a++) {
			stringOut.push(tasks.renderWidgetTaskElement(tasksToday[a],status));
		}
		stringOut.push('</div>');
		
        if (totalTasksTomorrow>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksTomorrow);
		} else {
			controlDisplay = 'none';
		}
		var tasksTomorrow = jsonObject.TasksTomorrow;
		header            = jsonObject.TasksTomorrowHeader;

        status = 'tomorrow';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="margin-top: 6px; display: '+controlDisplay+';">');
        stringOut.push('<div class="icon_calendar_day" style="margin: 3px; float: left;"><strong><span id="'+status+'_totalTasks">'+totalTasksTomorrow+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksTomorrow)-1);a++) {
			stringOut.push(tasks.renderWidgetTaskElement(tasksTomorrow[a],status));
		}
		stringOut.push('</div>');
		
        if (totalTasksThisWeek>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksThisWeek);
		} else {
			controlDisplay = 'none';
		}
		var tasksThisWeek = jsonObject.TasksThisWeek;
		header = jsonObject.TasksThisWeekHeader;

        status = 'thisWeek';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="margin-top: 6px; display: '+controlDisplay+';">');
        stringOut.push('<div class="icon_calendar_day" style="margin: 3px; float: left;"><strong><span id="'+status+'_totalTasks">'+totalTasksThisWeek+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksThisWeek)-1);a++) {
			stringOut.push(tasks.renderWidgetTaskElement(tasksThisWeek[a],status));
		}
		stringOut.push('</div>');
		
        if (totalTasksNextWeek>0) {
			controlDisplay = '';
            totalTasks = parseInt(totalTasks+totalTasksNextWeek);
		} else {
			controlDisplay = 'none';
		}
		var tasksNextWeek = jsonObject.TasksNextWeek;
		header        = jsonObject.TasksNextWeekHeader;

        status = 'nextWeek';

		stringOut.push('<div class="boxYellow clickable taskDisplayControl" style="margin-top: 6px; display: '+controlDisplay+';">');
        stringOut.push('<div class="icon_calendar_day" style="margin: 3px; float: left;"><strong><span id="'+status+'_totalTasks">'+totalTasksNextWeek+'</span> '+header+'</strong></div>');
        stringOut.push('<!--<a href="#" class="iconPrintSmall taskPrint" style="float: right;" status="'+status+'"></a>-->');
        stringOut.push('<div style="clear: both;"></div>');
        stringOut.push('</div>');
		stringOut.push('<div class="taskWidgetContainer '+status+'" status="'+status+'" style="display: '+panelDisplayDefault+';">');
		for(a=0;a<=((totalTasksNextWeek)-1);a++) {
			stringOut.push(tasks.renderWidgetTaskElement(tasksNextWeek[a],status));
		}
		stringOut.push('</div>');
        var stringHTML = stringOut.join('');

        if (totalTasks<1) {
            stringHTML = '<div class="boxYellow"><div class="infoMessageSmall">'+jsonObject.NoDataMessage+'</div></div>';
        }
        
		$('#widgetContent_tasks').html(stringHTML);
        $('.taskWidgetContainer').alternateContainerRowColors().hiliteContainerRow();

        //widgetToolsObj.attachHandlersTasks();

        /*
		 * Look for project JSON
		 */
		/*
		if (projectListArray.length<1) {
			$.ajax({
				type:  "POST",
				url:   siteURL+'projects/ProjectView/getProjectsForSelect',
                dataType: 'json',
				success: function(payload) {
					projectListArray = payload;
                    widgetToolsObj.attachHandlersTaskForm();
				}
			});
		} else {
            widgetToolsObj.attachHandlersTaskForm();
        }
        */
	},
	renderWidgetTaskElement : function(taskArray,status) {
        var widgetHTML = [];
        widgetHTML.push('<div class="containerTask_'+taskArray.TaskID+' row" style="width: 98%;">');
        widgetHTML.push('<input type="hidden" id="milestoneIDContainer_'+taskArray.TaskID+'" value="'+taskArray.ParentTaskID+'" />');
        widgetHTML.push('<input type="hidden" id="projectIDContainer_'+taskArray.TaskID+'" value="'+taskArray.ProjectID+'" />');
        widgetHTML.push('<input type="hidden" id="taskDateContainer_'+taskArray.TaskID+'" value="'+taskArray.DateEndRender+'" />');
        
        widgetHTML.push('<span>');
        if (taskArray.TaskNo>0) {
            widgetHTML.push('<strong>['+taskArray.TaskNo+']</strong> ');
        }
        widgetHTML.push('<a href="'+taskArray.TaskLink+'" id="taskTitle_'+taskArray.TaskID+'">'+taskArray.Title+'</a>');
        if (taskArray.ProjectID>0) {
            widgetHTML.push(' for <a href="'+SITE_URL+'projects/ProjectDetail/index/'+taskArray.ProjectID+'">'+taskArray.ProjectTitle+'</a></span>');
        }
        widgetHTML.push('<br /><a class="buttonEditSmall editTask" status="'+status+'" id="editTask_'+taskArray.TaskID+'" title="Edit" href="#"></a>');
        widgetHTML.push('<a class="buttonDeleteSmall deleteTask" status="'+status+'" id="deleteTask_'+taskArray.TaskID+'" title="Delete" href="#"></a>');
        widgetHTML.push('<a class="buttonCheckSmall chkTask" status="'+status+'" id="chkTask_'+taskArray.TaskID+'" href="#" title="Complete"></a> ');
        var returnWidgetHTML = widgetHTML.join('');
        return returnWidgetHTML;
    }
}	