var database = {
	createDatabase : function() {
		var appDB = Ti.Database.open('cpo');
		var sqlLogin  = "CREATE TABLE IF NOT EXISTS cpo_login ";
		 	sqlLogin += "(ID INTEGER PRIMARY KEY, Email TEXT, Password TEXT, AuthKey TEXT, Permissions TEXT, UserType INTEGER, KeepLoggedIn INTEGER);";
		appDB.execute(sqlLogin);
		
		var sqlTimesheet  = "CREATE TABLE IF NOT EXISTS cpo_timesheet ";
		    sqlTimesheet += "(ID INTEGER PRIMARY KEY, ProjectID INTEGER, ProjectTitle TEXT, TaskID INTEGER, ";
		    sqlTimesheet += "TaskTitle TEXT, Start INTEGER, Stop INTEGER, ElapsedTime REAL, DateClockStart TEXT, ";
		    sqlTimesheet += "DateClockEnd TEXT, Billable INTEGER, Comments TEXT, ClientCo TEXT, Sync INTEGER);"
		appDB.execute(sqlTimesheet);    
		
		appDB.close();	
	}
}	